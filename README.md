
## Missing slide


Running this:

```r
pagedown::chrome_print("lecture13_tidy_models.Rmd")  
```

gives a HTML with 3 slides but the second one (slide-practical) is missing in the PDF version

I have used `flipbookr` in several places, it is the first time I observed this.

```
sessioninfo::session_info()
#> ─ Session info ───────────────────────────────────────────────────────────────
#>  setting  value                       
#>  version  R version 4.0.3 (2020-10-10)
#>  os       Ubuntu 20.10                
#>  system   x86_64, linux-gnu           
#>  ui       X11                         
#>  language en                          
#>  collate  en_US.UTF-8                 
#>  ctype    en_US.UTF-8                 
#>  tz       Europe/Zurich               
#>  date     2021-05-17                  
#> 
#> ─ Packages ───────────────────────────────────────────────────────────────────
#>  package     * version date       lib source                           
#>  backports     1.2.1   2020-12-09 [1] CRAN (R 4.0.3)                   
#>  cli           2.5.0   2021-04-26 [1] CRAN (R 4.0.3)                   
#>  crayon        1.4.1   2021-02-08 [1] CRAN (R 4.0.3)                   
#>  digest        0.6.27  2020-10-24 [1] CRAN (R 4.0.3)                   
#>  ellipsis      0.3.2   2021-04-29 [1] CRAN (R 4.0.3)                   
#>  evaluate      0.14    2019-05-28 [1] CRAN (R 4.0.0)                   
#>  fansi         0.4.2   2021-01-15 [1] CRAN (R 4.0.3)                   
#>  fs            1.5.0   2020-07-31 [1] CRAN (R 4.0.2)                   
#>  glue          1.4.2   2020-08-27 [1] CRAN (R 4.0.2)                   
#>  highr         0.9     2021-04-16 [1] CRAN (R 4.0.3)                   
#>  htmltools     0.5.1.1 2021-01-22 [1] CRAN (R 4.0.3)                   
#>  httpuv        1.6.1   2021-05-07 [1] CRAN (R 4.0.3)                   
#>  jsonlite      1.7.2   2020-12-09 [1] CRAN (R 4.0.3)                   
#>  knitr         1.33    2021-04-24 [1] CRAN (R 4.0.3)                   
#>  later         1.2.0   2021-04-23 [1] CRAN (R 4.0.3)                   
#>  lifecycle     1.0.0   2021-02-15 [1] CRAN (R 4.0.3)                   
#>  magrittr      2.0.1   2020-11-17 [1] CRAN (R 4.0.3)                   
#>  pagedown      0.14.6  2021-05-17 [1] Github (rstudio/pagedown@f9779e3)
#>  pillar        1.6.1   2021-05-16 [1] CRAN (R 4.0.3)                   
#>  pkgconfig     2.0.3   2019-09-22 [1] CRAN (R 4.0.0)                   
#>  processx      3.5.2   2021-04-30 [1] CRAN (R 4.0.3)                   
#>  promises      1.2.0.1 2021-02-11 [1] CRAN (R 4.0.3)                   
#>  ps            1.6.0   2021-02-28 [1] CRAN (R 4.0.3)                   
#>  purrr         0.3.4   2020-04-17 [1] CRAN (R 4.0.0)                   
#>  R6            2.5.0   2020-10-28 [1] CRAN (R 4.0.3)                   
#>  Rcpp          1.0.6   2021-01-15 [1] CRAN (R 4.0.3)                   
#>  reprex        2.0.0   2021-04-02 [1] CRAN (R 4.0.3)                   
#>  rlang         0.4.11  2021-04-30 [1] CRAN (R 4.0.3)                   
#>  rmarkdown     2.8     2021-05-07 [1] CRAN (R 4.0.3)                   
#>  servr         0.22    2021-04-14 [1] CRAN (R 4.0.3)                   
#>  sessioninfo   1.1.1   2018-11-05 [1] CRAN (R 4.0.1)                   
#>  stringi       1.6.2   2021-05-17 [1] CRAN (R 4.0.3)                   
#>  stringr       1.4.0   2019-02-10 [1] CRAN (R 4.0.0)                   
#>  styler        1.3.2   2020-02-23 [1] CRAN (R 4.0.3)                   
#>  tibble        3.1.2   2021-05-16 [1] CRAN (R 4.0.3)                   
#>  utf8          1.2.1   2021-03-12 [1] CRAN (R 4.0.3)                   
#>  vctrs         0.3.8   2021-04-29 [1] CRAN (R 4.0.3)                   
#>  websocket     1.4.0   2021-04-23 [1] CRAN (R 4.0.3)                   
#>  withr         2.4.1   2021-01-26 [1] CRAN (R 4.0.3)                   
#>  xfun          0.23    2021-05-15 [1] CRAN (R 4.0.3)                   
#>  yaml          2.2.1   2020-02-01 [1] CRAN (R 4.0.0)                   
#> 
#> [1] /home/ginolhac/R/x86_64-pc-linux-gnu-library/4.0
#> [2] /usr/local/lib/R/site-library
#> [3] /usr/lib/R/site-library
#> [4] /usr/lib/R/library
```
